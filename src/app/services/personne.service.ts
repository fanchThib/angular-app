import { variable } from '@angular/compiler/src/output/output_ast';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class PersonneService {
  public personne = [
    {
        id: 1,
        nom: 'BRECHET',
        prenom: 'Sullivan ',
        status: 'codeur frontend',
        mdp: 'BRECHET',
    },
    {
        id: 2,
        nom: 'LEVIGOUROUX',
        prenom: 'Kevin',
        status: 'Alternant',
        mdp: 'LEVIGOUROUX',
    },
    {
        id:3,
        nom: 'LABARRE',
        prenom: 'Théo',
        status: 'Alternant',
        mdp: 'LABARRE',
    },
    {
        id:4,
        nom: 'MENARD',
        prenom: 'Chloe',
        status: 'Alternant',
        mdp: 'MENARD',
    },
    {
        id:5,
        nom: 'VIVION',
        prenom: 'Nicolat',
        status: 'Ingénieur en recherche & développement',
        mdp: 'VIVION',
    },
    {
        id:6,
        nom: 'THIBAUDEAU',
        prenom: 'François',
        status: 'stagiaire',
        mdp: 'THIBAUDEAU',
    },
    {
        id:7,
        nom: 'MOREAU',
        prenom: 'Alois',
        status: 'Chef de projet',
        mdp: 'MOREAU',
    },
    {
        id:8,
        nom: 'PAPA',
        prenom: 'Baptiste',
        status: 'lead tech',
        mdp: 'PAPA',
    }
];

getAlois(){
    console.log("affichage de l'index 0 (pour tester)");
    console.log(this.personne[0]);
    console.log("taille de mon tableau personne (pour tester)");
    console.log(this.personne.length);
}

getPersonneById(id: number) {
    console.log("=======getPersonneById=========== ");
    console.log("recherche avec l'id: "+id);
    const personne = this.personne.find(
      (s) => {
        return s.id === id;
      }
    );
    return personne;
}

getIdByMdp(pass: string){
    console.log("=======getIdByMdp=========== ");
    var id: number;
    console.log("-----s.mdp:"+this.personne[6].mdp+", pass:"+pass);
    console.log(this.personne[5]);
        for(var i = 0;i<this.personne.length;i++){
            console.log("-----s.mdp:"+this.personne[i].mdp+", pass:"+pass);
            if (this.personne[i].mdp === pass){
                console.log("+++++s.mdp:"+this.personne[i].mdp+", pass:"+pass);
                id = this.personne[i].id;
            }
          }
    return id;
}

  constructor() { }
}
