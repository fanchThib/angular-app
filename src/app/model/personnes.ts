export class Personne{
    id: number;
    prenom: string;
    nom: string;
    status: string;
    mdp:string;

    constructor(){
        this.id = 0;
        this.prenom = "";
        this.nom ="";
        this.status ="";
        this.mdp="";
    }
}