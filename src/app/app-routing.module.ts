import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthComponent } from './auth/auth.component';
import { PersonneViewComponent } from './personne-view/personne-view/personne-view.component';

const routes: Routes = [ { 
  path: '', redirectTo: 'personne', pathMatch: 'full'},
{ path: 'personne', component: PersonneViewComponent },
//{ path: 'personnes/:id', component: SinglePersonnesComponent },
{ path: 'auth', component: AuthComponent },];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
