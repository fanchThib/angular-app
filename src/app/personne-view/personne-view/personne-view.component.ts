import { Component, OnInit } from '@angular/core';
import { Personne } from 'src/app/model/personnes';
import { PersonneService } from 'src/app/services/personne.service';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-personne-view',
  templateUrl: './personne-view.component.html',
  styleUrls: ['./personne-view.component.css'],
  template: '<input [(ngModel)]="nom" (keyup.enter)="getAffichage()" />'
})


export class PersonneViewComponent implements OnInit {
  afi: string = "mdp";
  pers: Personne[] = [];
  personne: Personne = new Personne();
  mdp: string;

  OnNotify(pers: Personne){
    console.log("=============Coucou je suis OnNotify==============");
    this.personne = pers;
  }

  constructor(private personnesService: PersonneService) { }
  getPersonne(id: number){
    this.personne = this.personnesService.getPersonneById(id);
  }

  getSulivan(){
    this.personne = this.personnesService.getPersonneById(1);
  }
  getKevin(){
    this.personne = this.personnesService.getPersonneById(2);
  }
  getTheo(){
    this.personne = this.personnesService.getPersonneById(3);
  }
  getChloe(){
    this.personne = this.personnesService.getPersonneById(4);
  }
  getNicolat(){
    this.personne = this.personnesService.getPersonneById(5);
  }
  getFrancois(){
    this.personne = this.personnesService.getPersonneById(6);
  }
  getAlois(){
    this.personne = this.personnesService.getPersonneById(7);
  }
  getBaptiste(){
    this.personne = this.personnesService.getPersonneById(8);
  }
  
  ngOnInit(): void {
    console.log("=====coucou je suis le ngOnInit de personne-view-component====");
  }

  getAffichage(){
    console.log("=========getAffichage=========");
    console.log(this.personne);
  }

}
