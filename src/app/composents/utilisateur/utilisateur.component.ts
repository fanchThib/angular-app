import { stringify } from '@angular/compiler/src/util';
import { Component, EventEmitter, Output, OnInit, Input } from '@angular/core';
import { Personne } from 'src/app/model/personnes';
import { PersonneService } from 'src/app/services/personne.service';

@Component({
  selector: 'app-utilisateur',
  templateUrl: './utilisateur.component.html',
  styleUrls: ['./utilisateur.component.css']
})
export class UtilisateurComponent implements OnInit {

//variable pour les emission entre components
  @Input() notify:EventEmitter<Personne> = new EventEmitter<Personne>();

//variable de stockage
  nom: String;
  prenom: String;
  status: String;

//variable principal
  pers: Personne[] = [];
  personne: Personne = new Personne();

  constructor(private personnesService: PersonneService) { }

  ngOnInit(): void {
    var personneSaisi;
    var id;
    var pass;
    //TODO changer la variable pass par un system EventEmiter pour pouvoir se connecter depuis n'importe quel compte depuis l'application
    pass= 'THIBAUDEAU';

    personneSaisi = new Personne();
    id = this.personnesService.getIdByMdp(pass);
    
    console.log('============je teste mon id=============');
    console.log('mon id '+id);
    personneSaisi = this.personnesService.getPersonneById(id);

    //j'affecte mes variables nom, prenom et status depuis mon type perssonne atribuer a normal
    this.nom = personneSaisi.nom;
    this.prenom = personneSaisi.prenom;
    this.status = personneSaisi.status;

    this.personne = personneSaisi;

    console.log("on demare notifyEvent");
    this.notifyEvent();

  }

  notifyEvent(): void{
    console.log("===========coucou, je suis notifyEvent() (dans utilisateur component)============");
    this.notify.emit(this.personne);
  }

}
