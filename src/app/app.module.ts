//default import
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

//import pour le routage
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthComponent } from './auth/auth.component';
import { EventEmitter } from '@angular/core';

//import de component
import { PersonneViewComponent } from './personne-view/personne-view/personne-view.component';
import { UtilisateurComponent } from './composents/utilisateur/utilisateur.component';

//import pour les forms modules
import { FormsModule } from '@angular/forms';
import { from } from 'rxjs';


@NgModule({
  declarations: [
    AppComponent,
    AuthComponent,
    PersonneViewComponent,
    UtilisateurComponent,
    //EventEmitter,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
  ],
  providers: [
    //PersonnesService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
